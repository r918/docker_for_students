FROM ubuntu:18.04

MAINTAINER ASMik <cpp.create@gmail.com>

# Add user
RUN adduser --quiet --disabled-password qtuser

# Install Python 3, PyQt5
RUN apt-get update \
    && apt-get install -y \
          python3 \
          python3-pyqt5 \
          xvfb \
          python3-matplotlib \
          python3-pytest \
          python3-pytestqt \
          python3-qimage2ndarray

